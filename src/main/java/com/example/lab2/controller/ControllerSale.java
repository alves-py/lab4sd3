package com.example.lab2.controller;

import com.example.lab2.DTO.SaleRequestDTO;
import com.example.lab2.DTO.SaleResponseDTO;
import com.example.lab2.services.SaleService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vendas")
public class ControllerSale {
    private final SaleService saleService;


    public ControllerSale(SaleService saleService) {
        this.saleService = saleService;
    }

    @PostMapping
    public ResponseEntity<SaleResponseDTO> buySale(@RequestBody SaleRequestDTO requestDTO){
        return ResponseEntity.status(HttpStatus.CREATED).body(saleService.saleBuy(requestDTO));
    }

    @GetMapping
    public ResponseEntity<List<SaleResponseDTO>> salesAll(){
        return ResponseEntity.ok(saleService.saleGet());
    }
}
