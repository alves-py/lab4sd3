package com.example.lab2.repository;

import com.example.lab2.models.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductModel, Integer> {
    Optional<ProductModel> findByLabel(String label);
    Optional<ProductModel> findById(int id);
    Optional<ProductModel> findByPrice(double price);

}
