package com.example.lab2.repository;

import com.example.lab2.models.SaleModel;
import jakarta.persistence.Id;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SaleRepository extends JpaRepository<SaleModel, Id> {
}
