package com.example.lab2.models;

import com.example.lab2.DTO.ProductDTO;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="produtos")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String label;
    private double price;
    private int quantity;


    public ProductModel(ProductDTO productDTO){
        this.label = productDTO.label();
        this.price = productDTO.price();
        this.quantity = productDTO.quantity();

    }
}
