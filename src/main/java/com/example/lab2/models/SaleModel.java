package com.example.lab2.models;

import com.example.lab2.DTO.SaleRequestDTO;
import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity(name = "TB_SALES")
@Table(name = "TB_SALES")
@NoArgsConstructor
@Data
public class SaleModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private int productID;
    private int amount;
    private double finalValue;
    private LocalDateTime updatedAT;


    public SaleModel(SaleRequestDTO saleDTO){
        this.productID = saleDTO.productID();
        this.amount = saleDTO.amount();
        this.updatedAT = LocalDateTime.now();

    }

}
