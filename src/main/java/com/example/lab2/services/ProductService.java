package com.example.lab2.services;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.lab2.DTO.ProductDTO;
import com.example.lab2.models.ProductModel;
import com.example.lab2.repository.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public ProductModel updateProduct(int id, ProductDTO productUpdated){
        ProductModel updateProduct = repository.findById(id).orElseThrow(()->new EntityNotFoundException("Produto não encontrado, verifique se o ID está correto: " +id));

        if(productUpdated.label() != null) updateProduct.setLabel(productUpdated.label());
        if(productUpdated.price() > 0) updateProduct.setPrice(productUpdated.price());
        if(productUpdated.quantity() > 0) updateProduct.setQuantity(productUpdated.quantity());

        return repository.save(updateProduct);
    }
}
