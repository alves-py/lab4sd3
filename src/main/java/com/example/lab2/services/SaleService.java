package com.example.lab2.services;

import com.example.lab2.DTO.SaleRequestDTO;
import com.example.lab2.DTO.SaleResponseDTO;
import com.example.lab2.models.SaleModel;
import com.example.lab2.repository.ProductRepository;
import com.example.lab2.repository.SaleRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SaleService {
    private final SaleRepository saleRepository;
    private final ProductRepository productRepository;


    public SaleService(SaleRepository saleRepository, ProductRepository productRepository) {
        this.saleRepository = saleRepository;
        this.productRepository = productRepository;
    }

    public SaleResponseDTO saleBuy(SaleRequestDTO saleRequestDTO){
        if(saleTrue(saleRequestDTO)){
            var saleNew = new SaleModel(saleRequestDTO);
            saleNew.setFinalValue(verifyTotalPrice(saleRequestDTO));
            saleRepository.save(saleNew);
            updateQuantityProduct(saleRequestDTO);

            return new SaleResponseDTO(saleNew.getProductID(), saleNew.getAmount(), saleNew.getFinalValue(), saleNew.getUpdatedAT());
        }

        else throw new EntityNotFoundException("Desculpe. Cometemos um erro");
    }
    public List<SaleResponseDTO> saleGet(){
        var sales = saleRepository.findAll();

        return sales
                .stream()
                .map(sale -> new SaleResponseDTO(sale.getProductID(), sale.getAmount(),  sale.getFinalValue(), sale.getUpdatedAT()))
                .collect(Collectors.toList());
    }

    public boolean saleTrue(SaleRequestDTO requestSaleDTO){
        var product = productRepository.findById(requestSaleDTO.productID()).orElseThrow(()->new EntityNotFoundException("Produto não encontrado"));

        if(!(requestSaleDTO.amount() > 0)) throw new EntityNotFoundException("A quantidade não pode ser negativa");
        if(!(product.getQuantity() > 0)) throw new EntityNotFoundException("A quantidade não pode ser negativa");
        if(!(requestSaleDTO.amount() <= product.getQuantity()))throw new EntityNotFoundException("Estoque insuficiente.");

        return true;
    }

    public void updateQuantityProduct(SaleRequestDTO requestSaleDTO){
        var product = productRepository.findById(requestSaleDTO.productID()).orElseThrow(()->new EntityNotFoundException("Produto não encontrado"));

        if(product.getQuantity() > 0) product.setQuantity(product.getQuantity() - requestSaleDTO.amount());

        productRepository.save(product);
    }

    public double verifyTotalPrice(SaleRequestDTO requestSaleDTO) {

        var product = productRepository.findById(requestSaleDTO.productID()).orElseThrow(()->new EntityNotFoundException("Produto não encontrado"));

        double totalDiscount = 0;

        if(requestSaleDTO.amount() > 10)
            totalDiscount = 0.05;
        if(requestSaleDTO.amount() > 20)
            totalDiscount = 0.10;

        double totalPrice = requestSaleDTO.amount() * product.getPrice();

        return totalPrice - (totalPrice * totalDiscount);
    }
}
