package com.example.lab2.DTO;

public record ProductDTO(String label, float price, int quantity) {
}
