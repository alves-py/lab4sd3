package com.example.lab2.DTO;

public record SaleRequestDTO(int productID, int amount) {
}
