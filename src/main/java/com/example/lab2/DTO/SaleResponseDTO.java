package com.example.lab2.DTO;

import java.time.LocalDateTime;

public record SaleResponseDTO(int productID, int amount, double finalValue, LocalDateTime updatedAt) {
}
