# Readme - Projeto de Gerenciamento de Produtos e Vendas

Este é um projeto de API RESTful desenvolvido para gerenciar produtos e vendas em um banco de dados. A API oferece operações básicas de criação, leitura, atualização e exclusão (CRUD) de produtos, bem como métodos para pesquisar produtos por rótulo (label) e preço.

## Tecnologias Utilizadas

- **Spring Boot**: Framework utilizado para o desenvolvimento da aplicação.
- **Java**: Linguagem de programação principal do projeto.
- **Spring Data JPA**: Biblioteca para acesso a dados com Java Persistence API.
- **ElephantSQL Database**: Banco de dados online postgresql.
- **Maven**: Gerenciador de dependências e construção do projeto.

## Funcionalidades da API

- **Criação de Produto**: Permite adicionar um novo produto ao banco de dados.
- **Listagem de Produtos**: Retorna uma lista de todos os produtos armazenados no banco de dados.
- **Criação de vendas**: Permite adicionar uma nova venda ao banco de dados.
- **Listagem de vendas**: Retorna uma lista de todas as vendas no banco de dados.


Certifique-se de seguir corretamente a estrutura dos endpoints e os tipos de dados esperados para uma integração adequada com a API.

## Execução do Projeto

Para executar o projeto localmente, siga estas etapas:

1. Certifique-se de ter o Java JDK e o Maven instalados em seu sistema.
2. Clone este repositório em sua máquina local.
3. Navegue até o diretório raiz do projeto.
4. Execute o comando `mvn spring-boot:run` no terminal para iniciar o servidor.
5. Acesse os endpoints da API usando um cliente REST como Postman ou cURL.

